//
//  ViewController.swift
//  Animations
//
//  Created by Paul Pearson on 2/17/16.
//  Copyright © 2016 RPM Consulting. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    var running = false
    var curFrame = 1
    var timer = NSTimer()
    let frameCount = 16
    
    @IBOutlet var animatedImage: UIImageView!
    @IBOutlet var button: UIButton!
    
    
    @IBAction func updateImage(sender: AnyObject) {
        /*
        curFrame  = (++curFrame % frameCount) + 1
        let frameNum = String(format: "%02d", curFrame)
        animatedImage.image = UIImage(named: "frame\(frameNum).png")
        */
        if running {
            button.setTitle("Start Animation", forState: UIControlState.Normal)
            timer.invalidate()
            running = false
        } else {
            button.setTitle("Stop Animation", forState: UIControlState.Normal)
            timer = NSTimer.scheduledTimerWithTimeInterval(0.15, target: self, selector: Selector("doAnimation"), userInfo: nil, repeats: true)
            running = true
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func doAnimation() {
        curFrame  = (++curFrame % frameCount) + 1
        let frameNum = String(format: "%02d", curFrame)
        animatedImage.image = UIImage(named: "frame\(frameNum).png")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func viewDidLayoutSubviews() {
//        animatedImage.center = CGPointMake(animatedImage.center.x-400, animatedImage.center.y)
//        animatedImage.alpha = 0
//        animatedImage.frame = CGRectMake(100, 20, 0, 0)
    }

    override func viewDidAppear(animated: Bool) {
        
        UIView.animateWithDuration(2.5) { () -> Void in
//            self.animatedImage.center = CGPointMake(self.animatedImage.center.x+400, self.animatedImage.center.y)
//            self.animatedImage.alpha = 1
//            self.animatedImage.frame = CGRectMake(165, 100, 335, 557)
        }
    }
    
}

